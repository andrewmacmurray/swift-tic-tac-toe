//
//  Animator.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 10/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

struct Animator {
        
    static func springToOrigin(withDelay delay: TimeInterval, duration: TimeInterval, _ view: UIView) {
        UIView.animate(withDuration: duration,
                       delay: delay,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 1,
                       options: [],
                       animations: { view.translateY(0) },
                       completion: nil)
    }
    
    static func runStep(for duration: TimeInterval, step: @escaping () -> Void, onFinish: @escaping () -> Void) {
        UIView.animate(
            withDuration: duration,
            delay: 0,
            options: [.curveLinear],
            animations: step,
            completion: {_ in onFinish() }
        )
    }
    
    static func runStepWithSpring(for duration: TimeInterval, step: @escaping () -> Void, onFinish: @escaping () -> Void) {
        UIView.animate(
            withDuration: duration,
            delay: 0,
            usingSpringWithDamping: 0.7,
            initialSpringVelocity: 1,
            animations: step,
            completion: {_ in onFinish() }
        )
    }
    
}
