//
//  UIView+Transforms.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 11/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

extension UIView {
    
    func translateY(_ y: CGFloat) {
        self.transform = CGAffineTransform(translationX: 0, y: y)
    }
    
    func translateX(_ x: CGFloat) {
        self.transform = CGAffineTransform(translationX: x, y: 0)
    }
    
}
