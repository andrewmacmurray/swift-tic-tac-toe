//
//  TileButton.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 04/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

struct TileStyle {
    static let size    = 100
    static let spacing = 5
}

class TileButton: UIButton {
    
    private var tile: Tile
    private let sendMove: MoveCallback
    
    init(tile : Tile, onTap: @escaping MoveCallback) {
        self.tile = tile
        self.sendMove = onTap
        super.init(frame: CGRect.init())
        
        initialStyles()
        setFrame()
        enableTap()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder) not supported")
    }
    
    func refresh(_ tile : Tile) {
        self.tile = tile
        accessibilityIdentifier = tile.description
        setBackground()
    }
    
    func disableTap() {
        isUserInteractionEnabled = false
        removeTarget(nil, action: nil, for: .allEvents)
    }
    
    func enableTap() {
        isUserInteractionEnabled = true
        addTarget(self, action: #selector(onTap), for: .touchUpInside)
    }
    
    @objc func onTap(sender: TileButton) {
        if tile.isEmpty() {
            sendMove(tile.position())
        }
    }
    
    private func initialStyles() {
        accessibilityIdentifier = tile.description
        contentMode = .scaleAspectFit
        backgroundColor = Colors.lightPink
        layer.cornerRadius = 10
        layer.masksToBounds = true
    }
    
    private func setBackground() {
        switch tile {
        case let .Taken(_, p) where p == .X: highlightX()
        case let .Taken(_, p) where p == .O: highlightO()
        default: return
        }
    }
    
    private func highlightX() {
        withBorder(Colors.neonBlue)
        setBackground(Names.X)
    }
    
    private func highlightO() {
        withBorder(Colors.neonGreen)
        setBackground(Names.O)
    }
    
    private func withBorder(_ color: UIColor) {
        layer.borderColor = color.cgColor
        layer.borderWidth = 10
    }
    
    private func setBackground(_ image: String) {
        setBackgroundImage(UIImage(named: image), for: .normal)
    }
    
    private func setFrame() {
        let (x, y) = getCoordinates()
        let size   = TileStyle.size
        frame = CGRect(x: x, y: y, width: size, height: size)
    }
    
    private func getCoordinates() -> (Int, Int) {
        let fullSize = TileStyle.size + TileStyle.spacing
        let x = getColumn() * fullSize
        let y = getRow() * fullSize
        return (x, y)
    }
    
    private func getColumn() -> Int {
        return (tile.position() - 1) % 3
    }
    
    private func getRow() -> Int {
        let position = tile.position()
        if position > 6 {
            return 2
        } else if position > 3 {
            return 1
        } else {
            return 0
        }
    }
    
}
