//
//  ResultView.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 11/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import UIKit

class ResultView: UIView {
    
    private var image: UIImageView!
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder) not supported")
    }
    
    func showStatus(for status: Status, onComplete: @escaping () -> Void) {
        setImage(for: status)
        animate(onComplete: onComplete)
    }
    
    private func setup() {
        styleContainer()
        addImage()
    }
    
    private func setImage(for status: Status) {
        switch status {
        case .Win(.X): setImage("\(Names.X)Win")
        case .Win(.O): setImage("\(Names.O)Win")
        default:       setImage("Draw")
        }
    }
    
    private func setImage(_ name: String) {
        image.image = UIImage(named: name)
        image.accessibilityLabel = name
    }
    
    private func styleContainer() {
        let fullHeight = frame.height
        let fullWidth = frame.width
        frame  = CGRect(x: 0, y: 0, width: fullWidth, height: 300)
        center = CGPoint(x: fullWidth / 2, y: fullHeight / 2)
        backgroundColor = Colors.hotPink
        self.translateY(-fullHeight)
    }
    
    private func addImage() {
        image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.frame  = CGRect(x: 0, y: 0, width: 300, height: 250)
        image.center = CGPoint(x: frame.width / 2, y: frame.height / 2)
        image.translateY(-100)
        image.alpha  = 0
        
        addSubview(image)
    }
    
    private func animate(onComplete: @escaping () -> Void) {
        Animator.runStepWithSpring(for: 1, step: self.containerEnter, onFinish: {
            Animator.runStepWithSpring(for: 1, step: self.showImage, onFinish: onComplete)
        })
    }
    
    private func containerEnter() {
        self.translateY(0)
    }
    
    private func showImage() {
        image.translateY(0)
        image.alpha = 1
    }
    
}
