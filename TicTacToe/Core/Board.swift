//
//  Board.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

typealias Tiles = [Int: Tile]

enum Status {
    case Win(PlayerSymbol)
    case Draw
    case NonTerminal
}

class Board {
    
    let tiles: Tiles
    static let size: Int = 3
    
    init(_ tiles: Tiles = Board.createEmptyTiles()) {
        self.tiles = tiles
    }
    
    func makeMove(at: Int, by: PlayerSymbol) -> Board {
        let nextTiles = createNextTiles(position: at, player: by)
        return Board(nextTiles)
    }
    
    func status() -> Status {
        if      hasWon(.X) { return .Win(.X) }
        else if hasWon(.O) { return .Win(.O) }
        else if noMoves()  { return .Draw }
        else               { return .NonTerminal }
    }
    
    func validMove(_ move: Int) -> Bool {
        return availableMoves().contains(move)
    }
    
    func availableMoves() -> Set<Int> {
        let moves = tiles.values
            .filter({ $0.isEmpty() })
            .map({ $0.position() })
        return Set(moves)
    }
    
    private func noMoves() -> Bool {
        return availableMoves().count == 0
    }
    
    private func createNextTiles(position: Int, player: PlayerSymbol) -> Tiles {
        var newTiles = tiles
        newTiles[position] = .Taken(position, player)
        return newTiles
    }
    
    private static func createEmptyTiles() -> Tiles {
        let n = size * size
        return (1...n).reduce(into: [:], { tiles, index in
            tiles[index] = .Empty(index)
        })
    }
    
    private func playerMoves(_ player: PlayerSymbol) -> Set<Int> {
        let moves = tiles.values
            .filter({ $0.isTakenBy(player) })
            .map({ $0.position() })
        return Set(moves)
    }
    
    private func hasWon(_ player: PlayerSymbol) -> Bool {
        return winningStates.contains(where: { state in
            isWinState(playerMoves: playerMoves(player), winState: state)
        })
    }
    
    private func isWinState(playerMoves: Set<Int>, winState: Set<Int>) -> Bool {
        return winState.isSubset(of: playerMoves)
    }
    
    private let winningStates: Set<Set<Int>> = [
        [1, 2, 3], [4, 5, 6], [7, 8, 9],
        [1, 4, 7], [2, 5, 8], [3, 6, 9],
        [1, 5, 9], [3, 5, 7]
    ]
    
}
