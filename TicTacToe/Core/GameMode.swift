//
//  GameMode.swift
//  TicTacToe
//
//  Created by Andrew MacMurray on 06/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

enum GameMode {
    
    case HumanVsHuman
    case HumanVsComputer
    case ComputerVsComputer
    
    func create() -> Game {
        switch self {
        case .HumanVsHuman:       return makeGame(.human(.X),    .human(.O))
        case .HumanVsComputer:    return makeGame(.human(.X),    .computer(.O))
        case .ComputerVsComputer: return makeGame(.computer(.X), .computer(.O))
        }
    }
    
    private func makeGame(_ p1: Player, _ p2: Player) -> Game {
        return Game(players: Players(
            player1: p1,
            player2: p2
        ))
    }

}


