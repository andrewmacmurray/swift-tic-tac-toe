# Swift Tic Tac Toe

A Rupaul's Drag Race themed iOS version of Tic Tac Toe built in Swift

## Get Up and running

### Required Installs

Make sure you have [xcode](https://developer.apple.com/xcode/) installed (preferably verion 10)

Clone the repo with

```sh
> git clone https://gitlab.com/anaplicative/swift-tic-tac-toe.git
```

### Run in the Simulator

Open the project in XCode

Run the app in the simulator with `Cmd + R`

### Run the Test Suites

Run the unit and integration tests with `Cmd + U`
