//
//  GameTests.swift
//  TicTacToeTests
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest
@testable import TicTacToe

class GameTests: XCTestCase {
    
    func testInitGame() {
        let game = GameHelper.makeGame(moveSequence: [])
        
        XCTAssertEqual(game.currentPlayer.symbol, .X)
        XCTAssertEqual(game.board.tiles.count, 9)
    }
    
    func testNextMove() {
        let game = GameHelper.makeGame(moveSequence: [])
        
        game.nextMove(1)
        
        BoardHelper.assertTakenTileAtPosition(game.board.tiles, at: 1, player: .X)
    }
    
    func testMutlipleMoves() {
        let game = GameHelper.makeGame(moveSequence: [1, 4])
        
        BoardHelper.assertTakenTileAtPosition(game.board.tiles, at: 1, player: .X)
        BoardHelper.assertTakenTileAtPosition(game.board.tiles, at: 4, player: .O)
    }
    
    func testCannotMakeInvalidMove() {
        let game = GameHelper.makeGame(moveSequence: [1, 2, 1, 1])
        
        let remainingTiles = game.board.tiles.values.filter({ $0.isEmpty() })
        XCTAssertEqual(remainingTiles.count, 7)
    }
    
    func testGameNonTerminal() {
        let game = GameHelper.makeGame(moveSequence: [1, 2, 3])
        
        BoardHelper.assertNonTerminal(game.status)
    }
    
    func testPlayer1Win() {
        let game = GameHelper.makeGame(moveSequence: [1, 4, 2, 5, 3])
    
        BoardHelper.assertPlayerWin(game.status, .X)
    }
    
}
