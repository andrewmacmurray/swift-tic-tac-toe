//
//  BoardHelper.swift
//  TicTacToeTests
//
//  Created by Andrew MacMurray on 04/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest
@testable import TicTacToe

struct BoardHelper {

    static func assertDraw(_ status: Status) {
        if case .Draw = status {}
        else { XCTFail("Board is not draw") }
    }
    
    static func assertPlayerWin(_ status: Status, _ player: PlayerSymbol) {
        if case .Win(let p) = status {
            XCTAssertEqual(player, p)
        } else { XCTFail("Player has not won") }
    }
    
    static func assertNonTerminal(_ status: Status) {
        if case .NonTerminal = status {}
        else { XCTFail("invalid board status") }
    }
    
    static func assertEmptyTileAtPosition(_ tiles: Tiles, at: Int) {
        if case .some(.Empty(let i)) = tiles[at] {
            XCTAssertEqual(i, at)
        } else {
            XCTFail("incorrect empty tile value")
        }
    }
    
    static func assertTakenTileAtPosition(_ tiles: Tiles, at: Int, player: PlayerSymbol) {
        if case .some(.Taken(let i, let p)) = tiles[at] {
            XCTAssertEqual(i, at)
            XCTAssertEqual(p, player)
        } else {
            XCTFail("incorrect taken tile value")
        }
    }
    
    static func playMoves(_ moves : [Int]) -> Board {
        var player : PlayerSymbol = .X
        var board = Board()
        for position in moves {
            board  = board.makeMove(at: position, by: player)
            player = player.opponent()
        }
        return board
    }

}
