//
//  HumanVsHumanUITests.swift
//  TicTacToeUITests
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest

class HumanVsHumanUITests: GameUITestCase {

    override func setUp() {
        super.setUp()
        app.launch()
    }

    func testBoard() {
        // setup
        humanVhumanGame()
        
        // assertions
        assertBoardVisible()
        assertNineButtons()
        assertFirstTapTakesTile()
        assertSecondTapTakesAlternateTile()
        assertCannotRetakeTile()
    }
    
    private func humanVhumanGame() {
        app.buttons["Human Vs Human"].tap()
    }
    
    private func assertBoardVisible() {
        XCTAssertTrue(app.otherElements["boardView"].exists, "board should be visible")
    }
    
    private func assertNineButtons() {
        for i in 1...9 {
            let buttonName = "Tile-\(i)"
            XCTAssertTrue(app.buttons[buttonName].exists, "\(buttonName) should be visible")
        }
    }
    
    private func assertFirstTapTakesTile() {
        let empty = "Tile-1"
        let taken = "Tile-1-X"
        
        app.buttons[empty].tap()
        XCTAssertTrue(app.buttons[taken].exists, "tile 1 should have been taken by X")
    }
    
    private func assertSecondTapTakesAlternateTile() {
        let empty = "Tile-2"
        let taken = "Tile-2-O"
        
        app.buttons[empty].tap()
        XCTAssertTrue(app.buttons[taken].exists, "tile 2 should have been taken by O")
    }
    
    private func assertCannotRetakeTile() {
        let taken = "Tile-2-O"
        let retaken = "Tile-2-X"
        
        app.buttons[taken].tap()
        XCTAssertFalse(app.buttons[retaken].exists, "tile should not be retaken")
    }

}
