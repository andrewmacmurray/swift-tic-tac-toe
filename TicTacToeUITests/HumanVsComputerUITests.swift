//
//  HumanVsHumanUITests.swift
//  TicTacToeUITests
//
//  Created by Andrew MacMurray on 03/09/2018.
//  Copyright © 2018 Andrew MacMurray. All rights reserved.
//

import XCTest

class HumanVsComputerUITests: GameUITestCase {
    
    override func setUp() {
        super.setUp()
        app.launch()
    }
    
    func testBoard() {
        // setup
        humanVComputerGame()
        
        // assertions
        assertBoardVisible()
        assertNineButtons()
        assertFirstTapTakesTile()
        assertTapsDisabledForComputerPlayer()
        assertTakeNextMove()
    }
    
    private func humanVComputerGame() {
        app.buttons["Human Vs Computer"].tap()
        
    }
    
    private func assertBoardVisible() {
        XCTAssertTrue(app.otherElements["boardView"].exists, "board should be visible")
    }
    
    private func assertNineButtons() {
        for i in 1...9 {
            let buttonName = "Tile-\(i)"
            XCTAssertTrue(app.buttons[buttonName].exists, "\(buttonName) should be visible")
        }
    }
    
    private func assertFirstTapTakesTile() {
        let empty = "Tile-1"
        let taken = "Tile-1-X"
        
        app.buttons[empty].tap()
        XCTAssertTrue(app.buttons[taken].exists, "tile 1 should have been taken by X")
    }
    
    private func assertTapsDisabledForComputerPlayer() {
        let empty = "Tile-2"
        let taken = "Tile-2-O"

        app.buttons[empty].tap()
        XCTAssertFalse(app.buttons[taken].exists, "tile should have been disabled")
    }
    
    private func assertTakeNextMove() {
        let centerButton = shouldExist(app.buttons["Tile-5-O"])
        wait(for: [centerButton], timeout: 2)
    }
    
}
